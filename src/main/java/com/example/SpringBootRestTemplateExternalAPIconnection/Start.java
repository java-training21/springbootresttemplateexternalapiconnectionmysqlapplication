package com.example.SpringBootRestTemplateExternalAPIconnection;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//@SpringBootApplication
@Component
public class Start implements CommandLineRunner {

    private PersonRepository personRepository;

    @Autowired
    public Start(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }


    @Autowired
    private RestTemplate restTemplate;


    private static final String GET_PERSON_URL    = "http://localhost:8080/api/persons";
    private static final String GET_PERSON_ID_URL = "http://localhost:8080/api/persons/{id}";
    private static final String CREATE_PERSON_URL = "http://localhost:8080/api/persons";
    private static final String UPDATE_PERSON_URL = "http://localhost:8080/api/persons/{id}";
    private static final String DELETE_PERSON_URL = "http://localhost:8080/api/persons/{id}";

//    public static void main(String[] args) {
//        SpringApplication.run(Start.class, args);
//    }



    @Override
    public void run(String... strings) throws Exception {

//        createNewPerson();
//        getPersonById();
//        getAllPersons();
//        updatePerson();
//        getAllPersons();
//        deleteEmployee();
//        getAllPersons();

    }

    @EventListener(ApplicationReadyEvent.class) //umozliwia uruchomienie w momencie startu aplikacji                           GLOWNA METODA APLIKACJI
    public void runMethod() {

        //createNewPerson();   //zapis nowego
        getAllPersons();
        getPersonById(2);


        //Iterable<Person> all2 = personRepository.findAll(); //odczyt wszystkich
        //all2.forEach(System.out::println);

    }


    private void createNewPerson() {
        Person person = new Person("JavaXD", "Leader02", 30);
        RestTemplate restTemplate = new RestTemplate();
        Person result = restTemplate.postForObject(CREATE_PERSON_URL, person, Person.class);
        System.out.println(result);
    }

    //private void getPersonById() {
    private void getPersonById(int id) {
        //Map< String, String > params = new HashMap< String, String>() >();
        Map< String, Integer > params = new HashMap< String, Integer>();
        //params.put("id", "1");
        params.put("id", id);
        Person result = restTemplate.getForObject(GET_PERSON_ID_URL, Person.class, params);
        System.out.println(result);
    }

    private void getAllPersons() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> httpEntity = new HttpEntity ("parameters", headers);
        ResponseEntity< String > result = restTemplate.exchange(GET_PERSON_URL, HttpMethod.GET, httpEntity, String.class);
        System.out.println(result);
    }

    private void updatePerson() {
        Map < String, String > params = new HashMap < String, String > ();
        params.put("id", "1");
        Person updatePerson = new Person("Leader", "Java", 30);
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(UPDATE_PERSON_URL, updatePerson, params);
    }

    private void deleteEmployee() {
        Map < String, String > params = new HashMap();
        params.put("id", "1");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(DELETE_PERSON_URL, params);
    }

}
