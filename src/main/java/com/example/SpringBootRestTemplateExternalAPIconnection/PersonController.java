package com.example.SpringBootRestTemplateExternalAPIconnection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class PersonController {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    PersonService personService;

    @PostMapping("/persons")
    public void add(@RequestBody Person person) {
        personService.savePerson(person);
    }
    public Person createNewPerson(@RequestBody Person person) {  //@Valaid przed @ReequestBody
        return personRepository.save(person);
    }

    @GetMapping("/persons/{id}")
    public ResponseEntity<Person> getEmployeeById(@PathVariable(value = "id") Long personId)
            throws ResourceNotFoundException {
        Person person = personRepository.findById(personId)
                .orElseThrow(() -> new ResourceNotFoundException("Person not found for id : " + personId));
        return ResponseEntity.ok().body(person);
    }

    @GetMapping("/persons")
    public List<Person> getAllPersons() {
        return personRepository.findAll();
    }

    @PutMapping("/persons/{id}")
    public ResponseEntity<Person> updatePerson(@PathVariable(value = "id") Long personId, @Valid @RequestBody Person personData) throws ResourceNotFoundException {
        Person person = findPersonById(personId);
        person.setName(personData.getName());
        person.setSurname(personData.getSurname());
        person.setAge(personData.getAge());
        return ResponseEntity.ok(personRepository.save(person));
    }


    @DeleteMapping("/persons/{id}")
    public Map<String, Boolean> deletePerson(@PathVariable(value = "id") Long personId)
            throws ResourceNotFoundException {
        Person person = findPersonById(personId);
        personRepository.delete(person);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    private Person findPersonById(Long personId) throws ResourceNotFoundException {
        return personRepository.findById(personId).orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + personId));
    }
}